﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Shapes;
using System.Windows.Media;
using System.Windows.Controls;

namespace SnakeWPF.classes
{
    class Snake
    {
        
        public delegate void SnakeDelegate();
        public event SnakeDelegate SnakeDie;
        public event SnakeDelegate SnakeGrow;
        
        /// <summary>
        /// Размер змейки в диаметре 
        /// </summary>
        private int snakeSize;
        private int bodyElementCount;
        private int[] headPosition = new int[2];
        private List<Shape> snakeBody = new List<Shape>();
        private Shape snakeHead;
        private Direction direction;
        private int HomeWidth;
        private int HomeHeigth;
        


        #region 
        public Shape SnakeHead
        {
            get
            {
                return snakeHead;
            }

        }

        /// <summary>
        /// Размер элемента змейки 
        /// </summary> 
        /// 
        public int SnakeSize
        {
            get
            {
                return snakeSize;
            }
        }
        /// <summary>
        /// Количество элементов в теле змейки
        /// </summary> 
        /// 
        public int BodyElementCount
        {
            get
            {
                return bodyElementCount;
            }
            set
            {
                if (value > bodyElementCount)
                    bodyElementCount = value; 
            }
        }

        /// <summary>
        /// Список элементов тела змейки
        /// </summary>
        public List<Shape> SnakeBody
        {
            get
            {
                return snakeBody;
            }
        }

        /// <summary>
        /// Координаты головы змейки. [0] - по оси x, [1] - по оси y
        /// </summary>
        public int[] HeadPosition
        {
            get
            {
                return headPosition;
            }
        }
        #endregion 
        /// <summary>
        /// Создает экземпляр змейки
        /// </summary>
        /// <param name="GameZoneCellSize">Размер ячейки игрового поля</param>
        public Snake(int GameZoneCellSize, int GameFieldHeigth, int GameFeildWidth)
        {
            this.snakeSize = GameZoneCellSize;
            this.HomeHeigth = GameFieldHeigth;
            this.HomeWidth = GameFeildWidth;
            this.headPosition[0] = this.headPosition[1] = GameZoneCellSize * 20;
            this.direction = Direction.RIGTH;
            this.SnakeInit();
        }
        /// <summary>
        /// Инициализация змейки
        /// </summary>
        private void SnakeInit()
        {
            int[] BodyElementPosition = new int[2] {HeadPosition[0], HeadPosition[1]};
            for (int i = 2; i >0; i--)
            {
                BodyElementPosition[1] = HeadPosition[1] - i* SnakeSize;
                snakeBody.Add(ElementCreater.GetSnakeBody(SnakeSize,BodyElementPosition));
            }

            snakeHead = ElementCreater.GetSnakeHead(SnakeSize, headPosition);
        }

        /// <summary>
        /// Возвращает новый элемент тела змейки 
        /// </summary>
        private  Shape GetNewSnakeBodyElement()
        {
            return  ElementCreater.GetNewSnakeTail(this.snakeSize, this.SnakeBody);
        }

        public void MoveAndEat(Shape obj)
        {
            if (!this.HitTail()&&!this.HitWall())
            {
                if (ElementCreater.ComparePositions(obj, this.GetNextHeadPossition()[0], this.GetNextHeadPossition()[1]))
                {
                    Shape NewTail = ElementCreater.GetSnakeBody(SnakeBody.First());
                    SnakeBody.Add(NewTail);
                    ElementCreater.CanvasSetPosition(NewTail, HeadPosition[0], HeadPosition[1]);
                    SetNextHeadPosition();
                    ElementCreater.CanvasSetPosition(SnakeHead, HeadPosition[0], HeadPosition[1]);
                    SnakeGrow();

                }
                else
                {
                    SnakeBody.Add(SnakeBody.First());
                    ElementCreater.CanvasSetPosition(SnakeBody.First(), HeadPosition[0], HeadPosition[1]);
                    SnakeBody.Remove(SnakeBody.First());                   
                    SetNextHeadPosition();
                    ElementCreater.CanvasSetPosition(SnakeHead, HeadPosition[0], HeadPosition[1]);
                    
                 }


            }
            else
            {
                SnakeDie();
            }

        }

        public Shape Grow()
        {
            return SnakeBody.Last();
        }

        private int[] GetNextHeadPossition()
        {
            int[] NextHeadPosition = new int[2] {HeadPosition[0],HeadPosition[1]};
            switch (direction)
            {
                case Direction.LEFT:
                    NextHeadPosition[1] -= SnakeSize;
                    break;
                case Direction.RIGTH:
                    NextHeadPosition[1] += SnakeSize;
                    break;
                case Direction.UP:
                    NextHeadPosition[0] -= SnakeSize;
                    break;
                case Direction.DOWN:
                    NextHeadPosition[0] += SnakeSize;
                    break;
                default:
                    return null;
            }
            return NextHeadPosition;
        }

        private void SetNextHeadPosition()
        {
            switch (direction)
            {
                case Direction.LEFT:
                    headPosition[1] -= SnakeSize;
                    break;
                case Direction.RIGTH:
                    headPosition[1] += SnakeSize;
                    break;
                case Direction.UP:
                    headPosition[0] -= SnakeSize;
                    break;
                case Direction.DOWN:
                    headPosition[0] += SnakeSize;
                    break;
                default:
                    return;
            }
        }
        public void ChangeDirection(Direction direction)
        {
            switch (this.direction)
            {
                case Direction.RIGTH:
                    if ((direction != this.direction) && (direction != Direction.LEFT))
                        {
                        this.direction = direction;
                        }
                    break;
                case Direction.LEFT:
                    if ((direction != this.direction) && (direction != Direction.RIGTH))
                    {
                        this.direction = direction;
                    }
                    break;
                case Direction.UP:
                    if ((direction != this.direction) && (direction != Direction.DOWN))
                    {
                        this.direction = direction;
                    }
                    break;
                case Direction.DOWN:
                    if ((direction != this.direction) && (direction != Direction.UP))
                    {
                        this.direction = direction;
                    }
                    break;
                default:return;
            }
        }

        public bool HitTail()
        {
            
            foreach (Shape BodyElement in snakeBody)
            {
                int[] BodyElementPosition = ElementCreater.GetElementPosition(BodyElement);
                if (ElementCreater.ComparePositions(BodyElementPosition[0], BodyElementPosition[1], this.GetNextHeadPossition()[0], this.GetNextHeadPossition()[1]))
                {
                    return true;
                }
            }
            return false;
        }

        public bool HitWall()
        {
            if ((ElementCreater.ComparePositions(this.GetNextHeadPossition()[0], - SnakeSize)) ||
               ((ElementCreater.ComparePositions(this.GetNextHeadPossition()[1], - SnakeSize)))||
               ((ElementCreater.ComparePositions(this.GetNextHeadPossition()[0], this.HomeHeigth))) ||
               ((ElementCreater.ComparePositions(this.GetNextHeadPossition()[1], this.HomeWidth))))
            {
                return true;
            }
            else { 
                return false;
            }

        }

        public bool Eat(Shape obj)
        {
            if (ElementCreater.ComparePositions(obj, this.GetNextHeadPossition()[0], this.GetNextHeadPossition()[1]))
            {
                SnakeGrow();
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
