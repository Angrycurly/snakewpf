﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SnakeWPF.classes
{
    static class CoordinateGenerator
    {

        public static int GetRandomX(int xMin, int xMax,int step)
        {
            int tempMax = xMax / step;
            int tempMin = xMin / step;
            Random rand = new Random();
            int x = rand.Next(tempMin, tempMax);
            return x * step;
        }

        public static int GetRandomY(int yMin, int yMax,int step)
        {
            int tempMax = yMax / step;
            int tempMin = yMin / step;
            Random rand = new Random();
            int y = rand.Next(tempMin, tempMax);
            return y * step;
        }

        public static int GetRandomX(int xMin, int xMax)
        {
            Random rand = new Random();
            int x = rand.Next(xMin, xMax);
            return x;
        }

        public static int GetRandomY(int yMin, int yMax)
        {
            Random rand = new Random();
            int y = rand.Next(yMin, yMax);
            return y;
        }
    }
}
