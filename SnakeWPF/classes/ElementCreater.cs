﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using System.Windows.Shapes;
using System.Windows.Controls;

namespace SnakeWPF.classes
{
    static class ElementCreater
    {   
        /// <summary>
        /// Цвет заливки головы змейки
        /// </summary>
        public static Color HeadColor = Colors.DarkGreen;
        /// <summary>
        /// Цвет заливки тела змейки
        /// </summary>
        public static Color BodyColor = Colors.ForestGreen;
        public static Color WallColor = Colors.DarkGray;
        public static Color FoodColor = Colors.Red;

        /// <summary>
        /// Возвращает объект "Элемент тела змейки"
        /// <param name="SnakeSize">Размер элемента</param>
        /// <param name="Position">Позиция элемента([0] - оси x, [1] - по оси y)</param>
        /// </summary>
        public static Ellipse GetSnakeBody(int SnakeSize, int[] Position)
        {
            Ellipse SnakeBody = new Ellipse();
            SnakeBody.Height = SnakeBody.Width = SnakeSize;
            SnakeBody.Fill = new SolidColorBrush(BodyColor);
            CanvasSetPosition(SnakeBody, Position[0], Position[1]);
            return SnakeBody;
        }

        public static Ellipse GetSnakeBody(Shape obj)
        {
            Ellipse SnakeBody = new Ellipse();
            SnakeBody.Height = SnakeBody.Width = obj.Width;
            SnakeBody.Fill = obj.Fill;
            CanvasSetPosition(SnakeBody, (int) Canvas.GetTop(obj),(int) Canvas.GetLeft(obj));
            return SnakeBody;
        }
        /// <summary>
        /// Возвращает объект "Голова змейки"
        /// </summary>
        /// <param name="SnakeSize">Размер элемента</param>
        /// <param name="Position">Позиция элемента([0] - оси x, [1] - по оси y)</param>
        public static Ellipse GetSnakeHead (int SnakeSize,int[] Position)
        {
            Ellipse SnakeHead = new Ellipse();
            SnakeHead.Height = SnakeHead.Width = SnakeSize;
            SnakeHead.Fill = new SolidColorBrush(HeadColor);
            CanvasSetPosition(SnakeHead, Position[0], Position[1]);
            return SnakeHead;       
        }

        public static Ellipse GetApple(int FoodSize, int[] Position)
        {
            Ellipse Apple = new Ellipse();
            Apple.Height = Apple.Width = FoodSize;
            Apple.Fill = new SolidColorBrush(FoodColor);
            CanvasSetPosition(Apple, Position[0], Position[1]);
            return Apple;
        }

        public static Ellipse GetNewSnakeTail(int SnakeSize,List<Shape> SnakeBody)
        {
            int[] NewElementPosition = new int[2];
            NewElementPosition[0] = (int) Canvas.GetTop(SnakeBody.First());
            NewElementPosition[1] = (int) Canvas.GetLeft(SnakeBody.First());
            return GetSnakeBody(SnakeSize, NewElementPosition);
        }


        public static Line GetWall(int x1, int x2, int y1, int y2)
        {
            Line wall = new Line();
            wall.X1 = x1;
            wall.X2 = x2;
            wall.Y1 = y1;
            wall.Y2 = y2;
            wall.Stroke = new SolidColorBrush(WallColor);
           // CanvasSetPosition(wall, 20, 20);
            return wall;
        }

        public static void CanvasSetPosition(Shape obj, int Top, int Left)
        {
            Canvas.SetTop(obj, Top);
            Canvas.SetLeft(obj, Left);
        }

        public static int[] GetElementPosition(Shape obj)
        {
            int top =  (int) Canvas.GetTop(obj);
            int left = (int) Canvas.GetLeft(obj);
            return new int[2] {top,left};
        }

        public static bool ComparePositions(Shape obj1, Shape obj2)
        {
           if ((Canvas.GetTop(obj1) == Canvas.GetTop(obj2))&&(Canvas.GetLeft(obj1)== Canvas.GetLeft(obj1)))
            {
                return true;
            } else
            {
                return false;
            }
        }

        public static bool ComparePositions(Shape obj, int TopPosition, int LeftPosition)
        {
            if ((TopPosition == Canvas.GetTop(obj)) && (LeftPosition == Canvas.GetLeft(obj)))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static bool ComparePositions(int TopPosition1, int LeftPosition1, int TopPosition2, int LeftPosition2)
        {
            if ((TopPosition1 == TopPosition2) && (LeftPosition1 == LeftPosition2))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static bool ComparePositions(int Position1, int Position2)
        {
            if (Position1 == Position2)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

    }
 }

