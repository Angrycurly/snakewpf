﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;
using System.Windows.Markup;

namespace SnakeWPF.classes
{
    class Timer
    {
        private DispatcherTimer timer;
        private  TimeSpan interval;
        private EventHandler evenT;
        private bool TimerState;
         

        public bool GetTimerState()
        {
            return TimerState;
        }

        public Timer(TimeSpan interval, EventHandler evn)
        {
            this.timer = new DispatcherTimer();
            this.interval = interval;
            this.evenT = evn;
        }

        private void ConfTimer()
        {
            if (timer.Interval != this.interval)
            {
                timer.Interval = this.interval;
                timer.Tick += evenT;
            }
        }
        public void Start()
        {
            this.ConfTimer();
            timer.Start();
            TimerState = true;
        }

        public void Stop()
        {
            timer.Stop();
            TimerState = false;
        }

        public void Reset()
        {
            this.timer.Stop();
            this.timer = new DispatcherTimer();
            this.ConfTimer();
            this.Start();
        }

        public void IncreseTimeInterval(TimeSpan step)
        {
            timer.Interval = timer.Interval + step;
        }

        public void ReduсeTimeInterval(TimeSpan step)
        {
            timer.Interval = timer.Interval - step;
        }
    }
}
