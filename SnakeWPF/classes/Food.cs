﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Shapes;

namespace SnakeWPF.classes
{
    class Food
    {
        private Shape apple;
        private int FoodSize;
        private int[] PossibleFoodPosition;

        public Shape Apple
        {
            get { return apple;}
        }
        
        //private List<Shape> foodList;
        private TimeSpan repeateRange;
        public Food(int FoodSize,int FieldHeigth,int FieldWidth,TimeSpan time)
        {
            this.repeateRange = time;
            this.FoodSize = FoodSize;
            this.PossibleFoodPosition = new int[2]{FieldHeigth,FieldWidth};
            this.AppleInit();
        }

        private void AppleInit()
        {
            int[] RandomPosition = new int[2] { CoordinateGenerator.GetRandomX(0, PossibleFoodPosition[0] - this.FoodSize,FoodSize),
                                                CoordinateGenerator.GetRandomX(0, PossibleFoodPosition[1] - this.FoodSize,FoodSize)};
            apple = ElementCreater.GetApple(this.FoodSize, RandomPosition);
        }

        public void Eat()
        {
            int[] RandomPosition = new int[2] { CoordinateGenerator.GetRandomX(0, PossibleFoodPosition[0] - this.FoodSize,FoodSize),
                                                CoordinateGenerator.GetRandomX(0, PossibleFoodPosition[1] - this.FoodSize,FoodSize)};
            ElementCreater.CanvasSetPosition(Apple, RandomPosition[0], RandomPosition[1]);
        }
    }
}
