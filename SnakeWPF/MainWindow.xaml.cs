﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

using SnakeWPF.classes;

namespace SnakeWPF
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private Field field = new Field();
        private Snake snake;
        private Timer timer;
        private Wall wall;
        private Food food;
        private int Score;
        private bool GameOver = false;
        public MainWindow()
        {
            WindowInit();

            InitializeComponent();

            StartGame();

            
        }


        private void onPressAnyKey(object sender, KeyEventArgs key)
        {
            switch (key.Key)
            {
                case Key.Up:
                    snake.ChangeDirection(Direction.UP);
                    break;
                case Key.Down:
                    snake.ChangeDirection(Direction.DOWN);
                    break;
                case Key.Right:
                    snake.ChangeDirection(Direction.RIGTH);
                    break;
                case Key.Left:
                    snake.ChangeDirection(Direction.LEFT);
                    break;
                case Key.P:
                    if (!GameOver)
                    {
                        if (timer.GetTimerState())
                        {
                            timer.Stop();
                        }
                        else
                        {
                            timer.Start();
                        }    
                    }
                    break;
                case Key.Enter:
                    if (GameOver)
                    {
                        StartGame();
                    }
                    break;
                default:
                    return;

            }
        }

        private void Timer_Tick(object sender, EventArgs e)
        {
            snake.MoveAndEat(food.Apple);
        }

        private void SnakeInit(Snake snake)
        {
            foreach (Shape BodyElement in snake.SnakeBody)
            {
                GameField.Children.Add(BodyElement);                
            }
            GameField.Children.Add(snake.SnakeHead);

            snake.SnakeDie += Snake_SnakeDie;
            snake.SnakeGrow += Snake_SnakeGrow;
        }

        private void Snake_SnakeGrow()
        {
            this.SetNewElement(snake.Grow());
            food.Eat();
            ScoreText.Text = Convert.ToString(++Score);
            timer.ReduсeTimeInterval(new TimeSpan(0, 0, 0, 0, 2));

        }

        private void Snake_SnakeDie()
        {
            timer.Stop();
            this.GameOver = true;
            //this.Focusable = false;
        }

        private void WallsInit(Wall wall)
        {
            foreach (Shape WallElement in wall.Walls)
            {
                GameField.Children.Add(WallElement);
            }
        }

        private void WindowInit()
        {
            this.Height = field.GetFieldHeigth();
            //this.SnapsToDevicePixels = true;
            this.ResizeMode = ResizeMode.NoResize;
            this.Width = field.GetFieldWidth();
            this.Title = "Змейка";
            this.Focus();
            this.KeyDown += onPressAnyKey;
            
        }

        private void FoodInit()
        {
            GameField.Children.Add(food.Apple);
        }

        private void SetNewElement(Shape obj)
        {
            GameField.Children.Add(obj);
        }

        private void StartGame() {
            GameField.Children.Clear();
            this.Score = 0;
            timer = new Timer(new TimeSpan(0, 0, 0, 0, 70), Timer_Tick);
            snake = new Snake(field.GetGameZoneCellSize(), field.GetGameFieldHeigth(), field.GetGameFieldWidth());
            wall = new Wall(0, 0, field.GetGameFieldWidth(), field.GetGameFieldHeigth());
            food = new Food(field.GetGameZoneCellSize(), field.GetGameFieldHeigth(), field.GetGameFieldWidth(), new TimeSpan(0, 0, 0, 10));
            GameField.Margin = new Thickness(field.GetGameFieldMarginLeft(), 0, field.GetGameFieldMarginLeft(), 0);

            ScoreText.Text = Convert.ToString(Score);

            SnakeInit(snake);

            WallsInit(wall);

            FoodInit();

            timer.Start();
        }
    }
}
