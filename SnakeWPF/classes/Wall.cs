﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Shapes;

namespace SnakeWPF.classes
{
    class Wall
    {
        private List<Shape> walls = new List<Shape>();


        public List<Shape> Walls
        {
            get
            {
                return walls;
            }
        }

        public Wall(int x1,int y1, int x2, int y2)
        {
            walls.Add(ElementCreater.GetWall(x1, x2, y1, y1));
            walls.Add(ElementCreater.GetWall(x1, x2, y2, y2));
            walls.Add(ElementCreater.GetWall(x2, x2, y1, y2));
            walls.Add(ElementCreater.GetWall(x1, x1, y1, y2));
        }
    }
}
