﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;


namespace SnakeWPF.classes
{
    class Field
    {
        /// <summary>
        /// Количество ячеек по высоте
        /// </summary>
        private const int CellCount = 40;
        private int GameZoneStep;
        private int FieldHeigth;
        private int FieldWidth;
        private int GameFieldHeigth;
        private int GameFieldWidth;
        private int GameFieldMarginTop;
        private int GameFieldMarginLeft;
        private const int Margin = 5;

        //Формирует игровое поле
        public Field()
        {   
            this.FieldHeigth = (int) SystemParameters.PrimaryScreenHeight / 100 * (100 - 4 * Margin);
            this.FieldWidth = (int)SystemParameters.PrimaryScreenWidth / 100 * (100 - 4 * Margin);
            this.GameZoneStep = this.FieldHeigth / CellCount;
            this.GameFieldHeigth = ((this.FieldHeigth / 100 * (100 - 2 * Margin) / this.GameZoneStep) * this.GameZoneStep) ;
            this.GameFieldWidth = ((this.FieldWidth / 100 * (100 - 2 * Margin) / this.GameZoneStep) * this.GameZoneStep);
            this.GameFieldMarginTop = (this.FieldHeigth - this.GameFieldHeigth)/2;
            this.GameFieldMarginLeft = (this.FieldWidth - this.GameFieldWidth)/2;
        }  

        public int GetFieldHeigth()
        {
            return this.FieldHeigth;
        }

        public int GetFieldWidth()
        {
            return this.FieldWidth;
        }

        public int GetGameZoneCellSize()
        {
            return this.GameZoneStep;
        }

        public int GetGameFieldHeigth()
        {
            return this.GameFieldHeigth;
        }

        public int GetGameFieldWidth()
        {
            return this.GameFieldWidth;
        }

        public int GetGameFieldMarginTop()
        {
            return this.GameFieldMarginTop;
        }
        public int GetGameFieldMarginLeft()
        {
            return this.GameFieldMarginLeft;
        }
    }
}
